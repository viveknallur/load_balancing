'''
* Path: /home/lessismore/fabfile/

* Start running this file after the fabfile in /home/lessismore/ has begun running

* command to run: nohup fab experiment >& /dev/null < /dev/null &
* command to clean machine files: fab remove
* command to clean siegeResults files: fab clean

 This fabfile runs a siege test from 15 machines,
 for a specified length of time. The log files
 are stored in each machines 'results' dir, then
 copied to /home/lessismore/siegeResults and
 categorized into pool, and pool size log files.
'''

import time
from fabric import *
from fabric.api import *
from fabric.api import put, get

## Set Variables ##
siegeTime = 50
interval = 5.5
runs = 3

env.roledefs = {
	'loadgenerator': ['10.63.0.67', '10.63.0.68', '10.63.0.69', '10.63.0.70', '10.63.0.71', '10.63.0.74', '10.63.0.75', '10.63.0.76', '10.63.0.77', '10.63.0.78', '10.63.0.80', '10.63.0.81', '10.63.0.82', '10.63.0.83', '10.63.0.89']
	}

env.user = 'root'


@task
def experiment():
    
    # iterations
    for i in range(runs):
        
        # for each pool of each size, run siege test, and wait 'interval' for pool to switch
        for a in range(0, 8):
            execute(generate, a, i)
            time.sleep(interval)
                
        for b in range(8, 36):
            execute(generate, b, i)
            time.sleep(interval)
            
        for c in range(36, 92):
            execute(generate, c, i)
            time.sleep(interval)
                
        for d in range(92, 162):
            execute(generate, d, i)
            time.sleep(interval)
        
        for e in range(162, 218):
            execute(generate, e, i)
            time.sleep(interval)
                
        for f in range(218, 246):
            execute(generate, f, i)
            time.sleep(interval)
                
        for j in range(246, 254):
            execute(generate, j, i)
            time.sleep(interval)
                
        execute(generate, 254, i)
        time.sleep(interval)

    # download all files to lesssismore, and oranize
    execute(copy)
    organize()


# generate
@task
@parallel
@roles('loadgenerator')
def generate(y, iteration):
	run("siege -t" + str(siegeTime) + "S --log='/root/results/" + env.host + '_' + str(iteration) + '_' + namer(y) + ".log' -i -f url_list")


# download
@task
@parallel
@roles('loadgenerator')
def copy():
	get('/root/results/*', '../siegeResults/')


# organize
@task
def organize():
	for x in range(1, 9):
		local('(cd ../siegeResults/ ; cat *' + str(x) + '.log >> plots/size' + str(x) + '.log)')
	for y in range(255):
		local('(cd ../siegeResults/ ; cat *' + namer(y) + '.log >> ' + namer(y) + '.log)')
	local('(cd ../siegeResults/ ; rm 10.63*)')


# remove log files on machines
@task
@parallel
@roles('loadgenerator')
def remove():
	run('(cd results/ ; rm *.log)')


# remove log files in siegeResults
@task
def clean():
	local('(cd ../siegeResults/ ; rm *.log)')
	local('(cd ../siegeResults/plots/ ; rm *.log)')


# name pools
@task
def namer( num ):
	algos = ['f', 'h', 'l', 'rc', 'rr', 's', 'u', 'up']
	myalgos = []
	temp = []
	
	for i in range(1, 9):
		temp = temp + list(combinations(algos, i))
		for j in temp:
			j = ''.join(list(j)) + str(i)
			myalgos.append(j)
		temp = []
	return myalgos[num]

# combos
@task
def combinations(iterable, r):
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)			
