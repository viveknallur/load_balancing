'''
 * Path: /home/lessismore/

 * Start running this file first, then start running fabfile in /fabfile/ folder 

 * command to run: nohup fab experiment >& /dev/null < /dev/null & 

 This fabfile creates a list of all 255 pools, and cycles
 through the algorithms in each pool every 5 seconds as long
 as the corresponding siege files for that pool test do not exist.
 Once they do, the next pool begins rotating.
'''


import time
from fabric import *
from fabric.api import *

## Set Variables ##
interval = 5
runs = 3

env.roledefs = {
    'loadbalancer': ['10.63.0.88'],
    'loadgenerator': ['10.63.0.67', '10.63.0.68', '10.63.0.69', '10.63.0.70', '10.63.0.71', '10.63.0.74', '10.63.0.75', '10.63.0.76', '10.63.0.77', '10.63.0.78', '10.63.0.80', '10.63.0.81', '10.63.0.82', '10.63.0.83', '10.63.0.89'],
    }

env.user = 'root'

@task
def experiment():
    
    # Create list of all 255 pools
    algos = ['first', 'hdr', 'leastconn', 'rdp_cookie', 'roundrobin', 'source', 'uri', 'url-param']
    myalgos = []
    for i in range(1, 9):
        myalgos = myalgos + list(combinations(algos, i))
    
    
    # main loop
    for x in range(runs):
        
        # switch pools
        for y in range(255):
            index = 0
            execute(restartBalancer)
            
            # check for files, rotate algos every 5 seconds
            while (checkcheck(execute(check, y, x)) == 1):
                temporary = execute(replace, myalgos[y], index)
                index = temporary['10.63.0.88']
                execute(restartBalancer)
                time.sleep(interval)


# restart 
@task
@roles('loadbalancer')
def restartBalancer():
    run("sh safe_restart_haproxy.sh")


# copy algos
@task
@roles('loadbalancer')
def replace( pool, index ):
    run("cp configurations/%s haproxy.cfg" % pool[index])
    return index+1 if (index < (len(pool) - 1)) else 0


# check for files
@task
@parallel
@roles('loadgenerator')
def check(location, iteration):
    with settings(warn_only=True):
        if run("test -f ~/results/" + env.host + "_" + str(iteration) + "_" + namer(location) + ".log").failed:
            return 1
        else:
            return 0


# check every file
@task
def checkcheck(dict):
    if 1 in dict.values():
        return 1
    else:
        return 0


# combinations
@task
def combinations(iterable, r):
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)


# names of pools
@task
def namer( num ):
    algos = ['f', 'h', 'l', 'rc', 'rr', 's', 'u', 'up']
    myalgos = []
    temp = []

    for i in range(1, 9):
        temp = temp + list(combinations(algos, i))
        for j in temp:
           j = ''.join(list(j)) + str(i)
           myalgos.append(j)
        temp = []
    return myalgos[int(num)]


